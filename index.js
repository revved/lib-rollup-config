/* eslint-disable import/no-commonjs */
/* eslint-disable filenames/match-exported */
const resolve = require("rollup-plugin-node-resolve");
const commonjs = require("rollup-plugin-commonjs");
const json = require("rollup-plugin-json");
const terser = require("rollup-plugin-terser");
const babel = require("rollup-plugin-babel");
const toPascalCase = require("js-pascalcase");
const fs = require("fs");

const pkgPath = `${process.cwd()}/package.json`;
const pkg = JSON.parse(fs.readFileSync(pkgPath, "utf8"));

const configs = [];

if (pkg.browser) {
    configs.push({
        input: "lib/index.js",
        output: {
            name: toPascalCase(pkg.name),
            file: pkg.browser,
            format: "umd"
        },
        plugins: [
            resolve({
                browser: true,
                preferBuiltins: true
            }), // so Rollup can find `ms`
            commonjs(), // so Rollup can convert `ms` to an ES module,
            json(),
            babel({
                babelrc: false,
                runtimeHelpers: true,
                exclude: "node_modules/**",
                presets: [
                    [
                        "@babel/preset-env",
                        {
                            modules: false
                        }
                    ]
                ],
                plugins: [
                    [
                        "@babel/plugin-transform-runtime", // eslint-disable-line no-secrets/no-secrets
                        {
                            corejs: 2
                        }
                    ]
                ]
            }),
            terser.terser()
        ]
    });
}

if (pkg.main || pkg.module) {
    const mainAndMod = {
        input: "lib/index.js",
        external: ["ms"],
        output: [],
        plugins: [json()]
    };

    if (pkg.main) {
        mainAndMod.output.push({
            file: pkg.main,
            format: "cjs"
        });
    }

    if (pkg.module) {
        mainAndMod.output.push({
            file: pkg.module,
            format: "es"
        });
    }
}

module.exports = configs;
